﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Cwiczenia3.Models;

namespace Cwiczenia3
{
    /// <summary>
    /// Interaction logic for StudentEditDialog.xaml
    /// </summary>
    public partial class StudentEditDialog : Window
    {
        public StudentEditDialog(Student student)
        {
            InitializeComponent();
            this.ImieTextBox.Text = student.Imie;
            this.NazwiskoTextBox.Text = student.Nazwisko;
            this.NrIndeksuTextBox.Text = student.NrIndesku;

            this.CloseButton.Click += CloseButtonCloseOnClick;
        }
        private void CloseButtonCloseOnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
