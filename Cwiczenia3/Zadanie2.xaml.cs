﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace Cwiczenia3
{
    /// <summary>
    /// Interaction logic for Zadanie2.xaml
    /// </summary>
    public partial class Zadanie2 : Window
    {
        public Zadanie2()
        {
            InitializeComponent();

            List<string> statuses = new List<string>() {"aktywny", "nie aktywny", "zawieszony"};
            statuses.ForEach(x => Zadani2ComboBox.Items.Add(x));
            Zadani2ComboBox.Text = statuses.FirstOrDefault();

            Zadani2ComboBox.SelectionChanged += Zadanie2ComboBox_SelectionChanged;
            ExitButton.Click += ExitButton_OnClick;
        }

        private void Zadanie2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox comboBox)
            {
                var hasNext = e.AddedItems.GetEnumerator().MoveNext();
                {
                    Zadanie2TextBox.Text = e.AddedItems[0].ToString();
                }
            }
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
