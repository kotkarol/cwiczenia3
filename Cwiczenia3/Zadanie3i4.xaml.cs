﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Cwiczenia3.Models;
using MessageBox = System.Windows.MessageBox;

namespace Cwiczenia3
{

    /// <summary>
    /// Interaction logic for Zadanie3i4.xaml
    /// </summary>
    public partial class Zadanie3i4 : Window
    {
        private object _selectedRow; 

        public List<Student> students = new List<Student>();

        public Zadanie3i4()
        {
            InitializeComponent();
            students.Add(new Student()
            {
                Imie = "Test",
                Nazwisko = "Test",
                NrIndesku = "1"
            });
            students.Add(new Student()
            {
                Imie = "Test1",
                Nazwisko = "Test1",
                NrIndesku = "2"
            });

            Zadanie3i4DataGrid.DataContext = students;
            Zadanie3i4DataGrid.MouseDoubleClick += OpenStudentDetails;
            _selectedRow = Zadanie3i4DataGrid.SelectedValue;
            AddButton.Click += AddButton_Add;
            RemoveButton.Click += RemoveButton_Remove;

        }

        private void OpenStudentDetails(object sender, MouseButtonEventArgs e)
        {
            var selectedIndex = Zadanie3i4DataGrid.SelectedIndex;
            if (selectedIndex < 0)
            {
                MessageBox.Show("Nie wybrano  zadnego wiersza.", "Blad");
                return;
            }
            new StudentEditDialog(students.ElementAt(selectedIndex)).Show();
        }

        private void RemoveButton_Remove(object sender, RoutedEventArgs e)
        {
            students.Remove(students.ElementAt(Zadanie3i4DataGrid.SelectedIndex));
            Zadanie3i4DataGrid.DataContext = null;
            Zadanie3i4DataGrid.DataContext = students;
        }

        private void AddButton_Add(object sender, RoutedEventArgs e)
        {
            string imie = ImieTextBox.Text;
            string nazwisko = NazwiskoTextBox.Text;
            string nrIndeksu = NrIndeksuTextBox.Text;


            if (string.IsNullOrEmpty(imie))
            {
                MessageBox.Show("Nie wypelnione imie studenta!", "Blad");
                return;
            }
            if (string.IsNullOrEmpty(nazwisko))
            {
                MessageBox.Show("Nie wypelnione nazwisko studenta!", "Blad");
                return;
            }

            var match = Regex.Match(nrIndeksu, "^[s][0-9]{4}$");
            if (!match.Success)
            {
                MessageBox.Show("Bledny format numeru studenta." + Environment.NewLine +
                    "Numer powinen miec format \"sxxxx\" gdzie x to cyfra.", "Blad");
                return;
            }


            students.Add(new Student()
            {
                Imie = imie,
                Nazwisko = nazwisko,
                NrIndesku = nrIndeksu
            });
            ImieTextBox.Text = "";
            NazwiskoTextBox.Text = "";
            NrIndeksuTextBox.Text = "";

            Zadanie3i4DataGrid.DataContext = null;
            Zadanie3i4DataGrid.DataContext = students;
        }
    }
}
